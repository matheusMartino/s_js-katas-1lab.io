// Retornar os números de 1 a 20. (1, 2, 3,…, 19, 20)
function oneThroughTwenty() {
  let numeros = [];

  for (let i = 1; i <= 20; i++) {
    numeros.push(i);
  }
  console.log(numeros);
}
oneThroughTwenty();

// Retornar os números pares de 1 a 20. (2, 4, 6,…, 18, 20)
function evensToTwenty() {
  let numeros = [];

  for (let i = 2; i <= 20; i++) {
    if (i % 2 == 0) {
      numeros.push(i);
    }
  }
  console.log(numeros);
}
evensToTwenty();

// Retornar os números ímpares de 1 a 20. (1, 3, 5,…, 17, 19)
function oddsToTwenty() {
  let numeros = [];

  for (let i = 1; i <= 20; i++) {
    if (i % 2 != 0) {
      numeros.push(i);
    }
  }
  console.log(numeros);
}
oddsToTwenty();

// Retornar os múltiplos de 5 até 100. (5, 10, 15,…, 95, 100)
function multiplesOfFive() {
  let numeros = [];

  for (let i = 5; i <= 100; i++) {
    if (i % 5 == 0) {
      numeros.push(i);
    }
  }
  console.log(numeros);
}
multiplesOfFive();

// Retornar todos os números até 100 que forem quadrados perfeitos. (1, 4, 9, …, 81, 100)
function squareNumbers() {
  let numeros = [];
  let quadrado = 0;

  for (let i = 1; i <= 10; i++) {
    quadrado = i * i;
    numeros.push(quadrado);
  }
  console.log(numeros);
}
squareNumbers();

// Retornar os números contando de trás para frente de 20 até 1. (20, 19, 18, …, 2, 1)
function countingBackwards() {
  let numeros = [];

  for (let i = 20; i >= 1; i--) {
    numeros.push(i);
  }
  console.log(numeros);
}
countingBackwards();

// Retornar os números pares de 20 até 1. (20, 18, 16, …, 4, 2)
function evenNumbersBackwards() {
  let numeros = [];

  for (let i = 20; i >= 1; i--) {
    if (i % 2 == 0) {
      numeros.push(i);
    }
  }
  console.log(numeros);
}
evenNumbersBackwards();

// Retornar os números ímpares de 20 até 1. (19, 17, 15, …, 3, 1)

function oddNumbersBackwards() {
  let numeros = [];

  for (let i = 20; i >= 1; i--) {
    if (i % 2 != 0) {
      numeros.push(i);
    }
  }
  console.log(numeros);
}
oddNumbersBackwards();

// Retornar os múltiplos de 5 contando de trás para frente a partir de 100. (100, 95, 90, …, 10, 5)
function multiplesOfFiveBackwards() {
  let numeros = [];

  for (let i = 100; i >= 1; i--) {
    if (i % 5 == 0) {
      numeros.push(i);
    }
  }
  console.log(numeros);
}
multiplesOfFiveBackwards();

// Retornar os quadrados perfeitos contando de trás para frente a partir de 100. (100, 81, 64, …, 4, 1)
function squareNumbersBackwards() {
  let numeros = [];
  let quadrado = 0;

  for (let i = 10; i >= 1; i--) {
    quadrado = i * i;
    numeros.push(quadrado);
  }
  console.log(numeros);
}
squareNumbersBackwards();